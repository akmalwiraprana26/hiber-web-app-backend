﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Service.Implementation;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Extentions
{
    public static class TransactionExtension
    {
        public static IServiceCollection AddTransactionService(this IServiceCollection services)
        {
            services.AddTransient<ITransactionService, TransactionService>();
            return services;
        }
    }
}
