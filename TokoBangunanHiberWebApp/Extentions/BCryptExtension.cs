﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Service.Implementation;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Extentions
{
    public static class BCryptExtension
    {
        public static IServiceCollection AddBCryptService(this IServiceCollection services)
        {
            services.AddTransient<IBCryptHasher, BCryptHasher>();
            return services;
        }
    }
}
