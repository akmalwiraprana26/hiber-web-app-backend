﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Service.Implementation;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Extentions
{
    public static class SupplierExtension
    {
        public static IServiceCollection AddSupplierService(this IServiceCollection services)
        {
            services.AddTransient<ISupplierService, SupplierService>();
            return services;
        }
    }
}
