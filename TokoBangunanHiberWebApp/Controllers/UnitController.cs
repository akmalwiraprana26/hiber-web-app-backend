﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Category;
using TokoBangunanHiberWebApp.Model.Unit;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnitController : ControllerBase
    {
        private readonly IUnitService UnitService;

        public UnitController(IUnitService unitService)
        {
            this.UnitService = unitService;
        }

        [HttpGet("GetAllUnit")]
        public async Task<ActionResult<UnitPaginationModel>> GetAllUnit([FromQuery] FilterUnitListModel filter)
        {
            return await UnitService.GetAllUnit(filter);
        }

        [HttpGet("GetUnit/{unitId}")]
        public async Task<ActionResult<UnitViewModel>> GetUnit(int unitId)
        {
            return await UnitService.GetUnit(unitId);
        }

        [HttpGet("GetRemainingCategory")]
        public async Task<ActionResult<List<CategoryViewModel>>> GetRemainingCategory()
        {
            return await UnitService.GetRemainingCategory();
        }

        [HttpPost("UpdateUnit/{unitId}")]
        public async Task<ActionResult<UnitCreateUpdateResponseModel>> UpdateUnit([FromBody] UnitCreateUpdateModel form, int unitId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await UnitService.UpdateUnit(form, unitId);

            return result;
        }

        [HttpPost("CreateUnit")]
        public async Task<ActionResult<bool>> CreateUnit([FromBody] UnitCreateUpdateModel form)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return await UnitService.CreateUnit(form);
        }
    }
}
