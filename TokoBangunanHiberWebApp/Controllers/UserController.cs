﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.User;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService UserService;

        public UserController(IUserService userService)
        {
            this.UserService = userService;
        }

        [HttpPost]
        public async Task<ActionResult<bool>> CreateUser([FromBody] UserCreateModel form)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return await UserService.CreateUser(form);
        }

        [HttpGet("GetAllDriver")]
        public async Task<ActionResult<List<UserViewModel>>> GetAllDriver()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return await UserService.GetAllDriver();
        }

        [HttpGet("GetAllUser")]
        public async Task<ActionResult<UserPaginationModel>> GetAllUser([FromQuery] FilterUserListModel filter)
        {
            return await UserService.GetAllUser(filter);
        }

        [HttpGet("GetUser/{userId}")]
        public async Task<ActionResult<UserViewModel>> GetUser(Guid userId)
        {
            return await UserService.GetUser(userId);
        }

        [HttpPost("UpdateUser/{userId}")]
        public async Task<ActionResult<UserUpdateResponseModel>> UpdateUser([FromBody] UserUpdateModel form, Guid userId)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await UserService.UpdateUser(form, userId);

            return result;
        }

        [HttpPost("ChangePassword/{userId}")]
        public async Task<ActionResult<bool?>> ChangePassword(UserChangePasswordModel form, Guid userId)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return await UserService.ChangePassword(form, userId);
        }

    }
}
