﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.ReturProduct;
using TokoBangunanHiberWebApp.Model.Transaction;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService TransactionService;

        public TransactionController(ITransactionService transactionService)
        {
            this.TransactionService = transactionService;
        }

        [HttpGet]
        public async Task<ActionResult<TransactionPaginationModel>> GetAllTransaction([FromQuery] TransactionFilterModel filter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return await TransactionService.GetAllTransaction(filter);
        }

        [HttpGet("{transactionId:Guid}")]
        public async Task<ActionResult<TransactionDetailViewModel>> GetTransactionByTransactionId(Guid transactionId)
        {
            return await TransactionService.GetTransactionByTransactionId(transactionId);
        }

        [HttpPost("pay-credit-transaction/{transactionId:Guid}")]
        public async Task<ActionResult<bool>> PayCreditTransaction(Guid transactionId, [FromBody] TransactionPayModel form)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return await TransactionService.PayCreditTransaction(transactionId, form);
        }
    }
}
