﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Auth;
using TokoBangunanHiberWebApp.Service;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService AuthService;
        private readonly IUserService UserService;

        public AuthController(IAuthService authService, IUserService userService)
        {
            this.AuthService = authService;
            this.UserService = userService;
        }

        [HttpPost(Name = "TryLogin")]
        public async Task<ActionResult<UserSessionModel>> TryLogin([FromBody] LoginFormModel form)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userId =  await AuthService.IsVerified(form);

            if(userId == Guid.Empty)
            {
                return null;
            }

            var sessionData = await UserService.GetUserSessionByUserId(userId);

            return sessionData;
        }
    }
}
