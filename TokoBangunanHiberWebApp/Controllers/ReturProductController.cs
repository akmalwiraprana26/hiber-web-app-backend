﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.ReturProduct;
using TokoBangunanHiberWebApp.Model.Transaction;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReturProductController : ControllerBase
    {
        private readonly IReturProductService ReturProductService;

        public ReturProductController(IReturProductService returProductService)
        {
            this.ReturProductService = returProductService;
        }

        [HttpPost("retur-transaction")]
        public async Task<ActionResult<bool>> ReturTransaction([FromBody] ReturProductCreateModel form)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return await ReturProductService.ReturTransaction(form);
        }
    }
}
