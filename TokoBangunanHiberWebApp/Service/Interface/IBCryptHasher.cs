﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IBCryptHasher
    {
        string HashString(string text);
        bool Verify(string text, string hashedString);
    }
}
