﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Cashier;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface ICashierService
    {
        Task<CashierProductViewModel> GetProductByBarcodeIdOrName(string input);
        Task<Guid> SaveCashierData(CashierForm form);
    }
}
