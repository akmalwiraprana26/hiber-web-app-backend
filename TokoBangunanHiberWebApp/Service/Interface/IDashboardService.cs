﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Dashboard;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IDashboardService
    {
        Task<DashboardAdminViewModel> GetAdminDashboard();
        Task<DashboardEmployeeViewModel> GetEmployeeDashboard(Guid userId);
    }
}
