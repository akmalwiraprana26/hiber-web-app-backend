﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiberWebApp.Model;
using TokoBangunanHiberWebApp.Model.Product;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class ProductService : IProductService
    {
        private readonly HiberDBContext db;
        private static Random random;
        private readonly IWebHostEnvironment webHostEnvironment;

        public ProductService(HiberDBContext dbContext, IWebHostEnvironment hostEnvirontment)
        {
            this.db = dbContext;
            random = new Random();
            this.webHostEnvironment = hostEnvirontment;
        }

        
        public async Task<bool> CreateProduct(ProductEditCreateModel model)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var barcodeCode = new string(Enumerable.Repeat(chars, 10)
                .Select(s => s[random.Next(s.Length)]).ToArray());

            var existingBarcode = await db.Products
                                    .Where(Q => Q.BarcodeId == barcodeCode)
                                    .FirstOrDefaultAsync();

            while (existingBarcode != null)
            {
                barcodeCode = new string(Enumerable.Repeat(chars, 10)
                                .Select(s => s[random.Next(s.Length)]).ToArray());

                existingBarcode = await db.Products
                                    .Where(Q => Q.BarcodeId == barcodeCode)
                                    .FirstOrDefaultAsync();
            }

            var unit = await db.Units
                            .Where(Q => Q.CategoryId == model.ProductCategoryId)
                            .FirstOrDefaultAsync();

            var modeltest = model;
            
            
            var pr = new Product
            {
                BasePrice = model.ProductBasePrice,
                CategoryId = model.ProductCategoryId,
                Price = model.ProductPrice,
                UnitId = unit.UnitId,
                ProductName = model.ProductName,
                SupplierId = model.SupplierId,
                ProductId = Guid.NewGuid(),
                BarcodeId= barcodeCode,
                Stock = model.ProductStock,
                MinimumStockLimit = model.MinimumStockLimit
            };

            if(model.ImageFile != null)
            {
                model.ImageName = await SaveImage(model.ImageFile, pr.ProductId.ToString());
                pr.ImageName = model.ImageName;
            }
            else
            {
                pr.ImageName = null;
            }
            

            db.Products.Add(pr);
            await db.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteProduct(Guid productId)
        {
            var productToDelete = await db.Products
                                        .Where(Q => Q.ProductId == productId)
                                        .FirstOrDefaultAsync();

            if(productToDelete == null)
            {
                return false;
            }

            if(productToDelete.ImageName != null)
            {
                DeleteImage(productToDelete.ImageName);
            }

            db.Products.Remove(productToDelete);
            await db.SaveChangesAsync();

            return true;
        }

        public async Task<ProductPaginationModel> GetAllProduct(ProductFilterModel filter)
        {
            var data = new ProductPaginationModel();
            var dataProducts = await db.Products
                                    .Select(Q => new ProductViewModel
                                    {
                                        LastUpdated = Q.UpdatedAt,
                                        ProductBasePrice = Q.BasePrice,
                                        ProductCategoryId = Q.CategoryId,
                                        ProductID = Q.ProductId,
                                        ProductName = Q.ProductName,
                                        ProductPrice = Q.Price,
                                        ProductStock = Q.Stock,
                                        BarcodeId = Q.BarcodeId,
                                        CategoryName = Q.Category.CategoryName,
                                        SupplierId = Q.SupplierId,
                                        SupplierName = Q.Supplier.SupplierName,
                                        UnitName = Q.Unit.UnitName,
                                        MinimumStockLimit = Q.MinimumStockLimit ?? -1
                                    })
                                    .ToListAsync();

            if (!string.IsNullOrEmpty(filter.ProductCategory))
            {
                dataProducts = dataProducts.Where(Q => Q.CategoryName.ToLower().Contains(filter.ProductCategory.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(filter.ProductName))
            {
                dataProducts = dataProducts.Where(Q => Q.ProductName.ToLower().Contains(filter.ProductName.ToLower())).ToList();
            }
            data.TotalData = dataProducts.Count();
            if (filter.PageIndex != 0 && filter.ItemPerPage != 0)
            {
                dataProducts = dataProducts.Skip((filter.PageIndex - 1) * filter.ItemPerPage).Take(filter.ItemPerPage).ToList();
            }
            data.Products = dataProducts;
            

            return data;
        }

        public async Task<ProductViewModel> GetDetailProduct(Guid productId, RequestFileUrlModel urlModel)
        {
            var dataProduct = await db.Products
                                    .Where(Q => Q.ProductId == productId)
                                    .Select(Q => new ProductViewModel
                                    {
                                        LastUpdated = Q.UpdatedAt,
                                        ProductBasePrice = Q.BasePrice,
                                        ProductCategoryId = Q.CategoryId,
                                        ProductID = Q.ProductId,
                                        ProductName = Q.ProductName,
                                        ProductPrice = Q.Price,
                                        ProductStock = Q.Stock,
                                        SupplierId = Q.SupplierId,
                                        SupplierName = Q.Supplier.SupplierName,
                                        BarcodeId = Q.BarcodeId,
                                        CategoryName = Q.Category.CategoryName,
                                        ImageName = Q.ImageName,
                                        UnitName = Q.Unit.UnitName,
                                        MinimumStockLimit = Q.MinimumStockLimit ?? 1,
                                        ImageSrc = String.Format("{0}://{1}{2}/images/products/{3}",urlModel.Scheme,urlModel.Host,urlModel.PathBase,Q.ImageName)
                                    }).FirstOrDefaultAsync();

            return dataProduct;
        }

        public async Task<bool> UpdateProduct(ProductEditCreateModel model, Guid productId)
        {
            var product = await db.Products
                                .Where(Q => Q.ProductId == productId)
                                .FirstOrDefaultAsync();

            if(product == null)
            {
                return false;
            }

            if(model.ImageFile != null)
            {
                if(product.ImageName != null)
                {
                    DeleteImage(product.ImageName);
                }
                
                product.ImageName = await SaveImage(model.ImageFile, productId.ToString());
            }
            else
            {
                if(product.ImageName != null)
                {
                    DeleteImage(product.ImageName);
                }
                product.ImageName = null;
            }

            var unit = await db.Units
                            .Where(Q => Q.CategoryId == model.ProductCategoryId)
                            .FirstOrDefaultAsync();

            product.ProductName = model.ProductName;
            product.BasePrice = model.ProductBasePrice;
            product.CategoryId = model.ProductCategoryId;
            product.Price = model.ProductPrice;
            product.UnitId = unit.UnitId;
            product.UpdatedAt = DateTime.Now;
            product.Stock = model.ProductStock;
            product.MinimumStockLimit = model.MinimumStockLimit;

            await db.SaveChangesAsync();

            return true;
        }

        public async Task<CreateEditFormData> GetCreateEditFormData()
        {
            var existingUnit = await db.Units
                                .Select(Q => Q.CategoryId)
                                .ToListAsync();
            var categories = await db.Categories
                                    .Where(Q => existingUnit.Contains(Q.CategoryId))
                                   .Select(Q => new DropdownModel
                                   {
                                       label = Q.CategoryName,
                                       value = Q.CategoryId.ToString()
                                   }).ToListAsync();

            var suppliers = await db.Suppliers
                                  .Select(Q => new DropdownModel
                {
                                      label = Q.SupplierName,
                                      value = Q.SupplierId.ToString()
                }).ToListAsync();

            var data = new CreateEditFormData
            {
                CategoriesList = categories,
                SupplierList = suppliers
            };

            return data;
        }

        [NonAction]
        public async Task<string> SaveImage(IFormFile imageFile, string productId)
        {
            string imageName = new String(productId);
            imageName = imageName+Path.GetExtension(imageFile.FileName);
            string ext = Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine(webHostEnvironment.ContentRootPath, "Images/Products", imageName);
            using( var fileStream = new FileStream(imagePath,FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }
            return imageName;
        }
        [NonAction]
        public void DeleteImage(string imageName)
        {
            var imagePath = Path.Combine(webHostEnvironment.ContentRootPath, "Images/Products", imageName);
            if (System.IO.File.Exists(imagePath))
            {
                System.IO.File.Delete(imagePath);
            }
        }

        public async Task<List<ProductDropdownModel>> GetAllProductDropdown()
        {
            var products = await db.Products
                .AsNoTracking()
                .Select(Q => new ProductDropdownModel
                {
                    ProductID = Q.ProductId,
                    ProductName = Q.ProductName
                }).ToListAsync();
            return products;
        }

    }
}
