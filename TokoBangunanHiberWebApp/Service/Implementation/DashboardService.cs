﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiber.Entities.Enums;
using TokoBangunanHiberWebApp.Model.Dashboard;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class DashboardService : IDashboardService
    {
        public DashboardService(HiberDBContext db)
        {
            this.DB = db;
        }

        private readonly HiberDBContext DB;

        public async Task<DashboardAdminViewModel> GetAdminDashboard()
        {
            var unpaidTransactions = await (from t in DB.Transactions
                                            where t.PaymentTypeId == (int)PaymentTypeEnum.Kredit && t.IsPaid == false
                                            orderby t.CreatedAt descending
                                            select new TopUnpaidTransaction
                                            {
                                                TransactionId = t.TransactionId,
                                                BuyerName = t.BuyerName,
                                                DeadlineDate = ((DateTime)t.DeadlineDate).ToString("dd/MM/yyyy"),
                                                Interest = (Decimal)t.Interest,
                                                TotalPrice = (Decimal)t.TotalPrice,
                                                TotalUnpaid = t.TotalPrice - (from ph in DB.PaymentHistories
                                                                              where ph.TransactionId.Equals(t.TransactionId)
                                                                              select ph.Payment).Sum()
                                            }).ToListAsync();

            var outOfStockProducts = await (from p in DB.Products
                                            orderby p.Stock ascending
                                            where p.Stock < p.MinimumStockLimit
                                            select new TopOutOfStockProduct { 
                                                ProductId = p.ProductId,
                                                ProductModal = p.BasePrice,
                                                ProductLastUpdate = p.UpdatedAt.ToString("dd/MM/yyyy"),
                                                ProductName = p.ProductName,
                                                ProductPrice = p.Price,
                                                ProductStock = p.Stock
                                            }).ToListAsync();

            var needToBeDeliverDeliveries = await (from d in DB.Deliveries
                                                   join t in DB.Transactions on d.TransactionId equals t.TransactionId
                                                   join u in DB.Users on d.UserId equals u.UserId
                                                   where d.DeliveryStatusId == (int)DeliveryStatusEnum.MemuatBarang
                                                   orderby d.CreatedAt descending
                                                   select new TopNeedToBeDelivered
                                                   {
                                                       BuyerName = t.BuyerName,
                                                       DeliveryId = d.DeliveryId,
                                                       DeliveryStatusId = d.DeliveryStatusId,
                                                       DriverName = u.Username,
                                                       Address = d.Address,
                                                       OrderDate = d.CreatedAt.ToString("dd/MM/yyyy")
                                                   }).ToListAsync();

            var totalRevenue = await (from ph in DB.PaymentHistories
                                join t in DB.Transactions on ph.TransactionId equals t.TransactionId
                                select new
                                {
                                    ph.Payment
                                }).ToListAsync();

            var totalRevenueThisMonth = await (from ph in DB.PaymentHistories
                                         join t in DB.Transactions on ph.TransactionId equals t.TransactionId
                                         where ph.PaymentDate.Month == DateTime.Now.Month
                                         select new
                                         {
                                             ph.Payment
                                         }).ToListAsync();

            var totalRevenueToday = await (from ph in DB.PaymentHistories
                                               join t in DB.Transactions on ph.TransactionId equals t.TransactionId
                                               where ph.PaymentDate.Date == DateTime.Today
                                               select new
                                               {
                                                   ph.Payment
                                               }).ToListAsync();

            var totalFinancial = new TotalFinancial
            {
                TotalRevenue = totalRevenue.Sum(Q => Q.Payment),
                TotalRevenueThisMonth = totalRevenueThisMonth.Sum(Q => Q.Payment),
                TotalRevenueToday = totalRevenueToday.Sum(Q => Q.Payment)
            };

            return new DashboardAdminViewModel
            {
                NeedToBeDeliveredDeliveries = needToBeDeliverDeliveries,
                OutOfStockProducts = outOfStockProducts,
                TotalFinancial = totalFinancial,
                UnpaidTransactions = unpaidTransactions
            };
        }

        public async Task<DashboardEmployeeViewModel> GetEmployeeDashboard(Guid userId)
        {
            var outOfStockProducts = await (from p in DB.Products
                                            orderby p.Stock ascending
                                            where p.Stock < p.MinimumStockLimit
                                            select new TopOutOfStockProduct
                                            {
                                                ProductId = p.ProductId,
                                                ProductModal = p.BasePrice,
                                                ProductLastUpdate = p.UpdatedAt.ToString("dd/MM/yyyy"),
                                                ProductName = p.ProductName,
                                                ProductPrice = p.Price,
                                                ProductStock = p.Stock
                                            }).ToListAsync();

            var needToBeDeliverDeliveries = await (from d in DB.Deliveries
                                                   join t in DB.Transactions on d.TransactionId equals t.TransactionId
                                                   join u in DB.Users on d.UserId equals u.UserId
                                                   where d.DeliveryStatusId == (int)DeliveryStatusEnum.MemuatBarang && userId.Equals(u.UserId)
                                                   orderby d.CreatedAt descending
                                                   select new TopNeedToBeDelivered
                                                   {
                                                       BuyerName = t.BuyerName,
                                                       DeliveryId = d.DeliveryId,
                                                       DeliveryStatusId = d.DeliveryStatusId,
                                                       DriverName = u.Username,
                                                       Address = d.Address,
                                                       OrderDate = d.CreatedAt.ToString("dd/MM/yyyy")
                                                   }).ToListAsync();

            return new DashboardEmployeeViewModel
            {
                NeedToBeDeliveredDeliveries = needToBeDeliverDeliveries,
                OutOfStockProducts = outOfStockProducts,
            };
        }
    }
}
