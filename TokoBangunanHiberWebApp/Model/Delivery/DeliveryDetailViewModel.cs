﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Delivery
{
    public class DeliveryProductDetail
    {
        public string ProductName { get; set; }
        public int ProductQuantity  { get; set; }
        public string ProductUnitName { get; set; }
        public decimal ProductPrice { get; set; }
    }

    public class DeliveryDetailViewModel
    {
        public Guid TransactionId { get; set; }
        public string BuyerName { get; set; }
        public string Address { get; set; }
        public string DriverName { get; set; }
        public int StatusId { get; set; }
        public decimal TotalPrice { get; set; }
        public List<DeliveryProductDetail> ListBoughtProduct { get; set; }
    }
}
