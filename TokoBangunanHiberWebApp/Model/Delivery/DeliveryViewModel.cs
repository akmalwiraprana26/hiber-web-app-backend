﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Delivery
{
    public class DeliveryViewModel
    {
        public Guid DeliveryId { get; set; }
        public Guid DriverId { get; set; }
        public string BuyerName { get; set; }
        public string DriverName { get; set; }
        public int DeliveryStatusId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
