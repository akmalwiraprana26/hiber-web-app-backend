﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Purchase_Order
{
    public class DetailViewModel
    {
        public PurchaseOrderViewModel purchaseOrder { get; set; }
        public List<PurchaseOrderDetailViewModel> PurchaseOrderDetail { get; set; }
    }
}
