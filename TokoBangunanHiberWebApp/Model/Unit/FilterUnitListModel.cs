﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Unit
{
    public class FilterUnitListModel
    {
        public string UnitName { get; set; }
        public string CategoryName { get; set; }
        public int PageIndex { set; get; }
        public int ItemPerPage { set; get; }
    }
}
