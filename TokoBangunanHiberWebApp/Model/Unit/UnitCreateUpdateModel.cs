﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Unit
{
    public class UnitCreateUpdateModel
    {
        public int CategoryId { get; set; }
        public string UnitName { get; set; }
    }

    public class UnitCreateUpdateResponseModel
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
    }
}
