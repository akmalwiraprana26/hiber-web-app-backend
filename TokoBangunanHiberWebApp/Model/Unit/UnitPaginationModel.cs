﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Unit
{
    public class UnitPaginationModel
    {
        public List<UnitViewModel> Units { get; set; }
        public int TotalData { get; set; }
    }
}
