﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.ReturProduct
{
    public class ReturProductDataGridModel
    {
        public List<ReturProductViewModel> ReturProducts { get; set; }
        public int MaxPage { get; set; }
    }
}
