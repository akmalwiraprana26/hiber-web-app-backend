﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Transaction
{
    public class TransactionPaginationModel
    {
        public List<TransactionViewModel> Transactions { get; set; }
        public int TotalData { get; set; }
    }
}
