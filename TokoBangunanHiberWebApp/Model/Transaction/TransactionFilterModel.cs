﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Transaction
{
    public class TransactionFilterModel
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? PaymentTypeId { get; set; }
        public bool? IsPaid { get; set; }
        public string BuyerName { get; set; }
        public Guid? TransactionId { get; set; }
        public int PageIndex { set; get; }
        public int ItemPerPage { set; get; }
    }
}
