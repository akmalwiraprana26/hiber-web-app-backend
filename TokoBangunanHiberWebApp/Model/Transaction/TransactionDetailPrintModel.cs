﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Transaction
{
    public class TransactionDetailPrintModel
    {
        public int ProductQuantity { get; set; }
        public string ProductUnitName { get; set; }
        public string ProductName { get; set; }
        public decimal ProductTotalPrice { get; set; }
    }
}
