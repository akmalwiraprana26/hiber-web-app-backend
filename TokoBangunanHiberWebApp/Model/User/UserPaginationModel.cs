﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.User
{
    public class UserPaginationModel
    {
        public List<UserViewModel> Users { get; set; }
        public int TotalData { get; set; }
    }
}
