﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.User
{
    public class UserUpdateResponseModel
    {
        public bool IsSuccess { get; set; }
        public string ErrorMsg { get; set; }
    }

    public class UserUpdateModel
    {
        public Guid UpdatedUserId { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
    }
}
