﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Product
{
    public class PrintBarcodeFormModel
    {
        public List<BarcodeFormModel> ListBarcodeModel { get; set; }
        public float BarcodeWidth { get; set; }
        public int PageSizeEnum { get; set; }
    }

    public class BarcodeFormModel
    {
        public string ProductName { get; set; }
        public string ProductBase64BarcodeId { get; set; }
        public int Quantity { get; set; }
    }
}
