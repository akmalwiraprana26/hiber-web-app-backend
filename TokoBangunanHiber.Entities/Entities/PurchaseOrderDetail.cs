﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class PurchaseOrderDetail
    {
        public Guid PurchaseOrderDetailId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public Guid ProductId { get; set; }
        public Guid? PurchaseOrderId { get; set; }
        public int? ReturQuantity { get; set; }

        public virtual Product Product { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
    }
}
