﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class PurchaseOrder
    {
        public PurchaseOrder()
        {
            PurchaseOrderDetails = new HashSet<PurchaseOrderDetail>();
        }

        public Guid PurchaseOrderId { get; set; }
        public decimal TotalPrice { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ApprovedAt { get; set; }
        public Guid SupplierId { get; set; }
        public int PurchaseOrderStatusId { get; set; }
        public string SubmitBy { get; set; }
        public string Note { get; set; }

        public virtual PurchaseOrderStatus PurchaseOrderStatus { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<PurchaseOrderDetail> PurchaseOrderDetails { get; set; }
    }
}
