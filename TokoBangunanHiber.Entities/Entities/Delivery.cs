﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class Delivery
    {
        public Delivery()
        {
            ReturProducts = new HashSet<ReturProduct>();
        }

        public Guid DeliveryId { get; set; }
        public Guid TransactionId { get; set; }
        public Guid UserId { get; set; }
        public int DeliveryStatusId { get; set; }
        public string Address { get; set; }
        public DateTime CreatedAt { get; set; }

        public virtual DeliveryStatus DeliveryStatus { get; set; }
        public virtual Transaction Transaction { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ReturProduct> ReturProducts { get; set; }
    }
}
