﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class HiberDBContext : DbContext
    {
        public HiberDBContext(DbContextOptions<HiberDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Blob> Blobs { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Delivery> Deliveries { get; set; }
        public virtual DbSet<DeliveryStatus> DeliveryStatuses { get; set; }
        public virtual DbSet<PaymentHistory> PaymentHistories { get; set; }
        public virtual DbSet<PaymentType> PaymentTypes { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<PurchaseOrder> PurchaseOrders { get; set; }
        public virtual DbSet<PurchaseOrderDetail> PurchaseOrderDetails { get; set; }
        public virtual DbSet<PurchaseOrderStatus> PurchaseOrderStatuses { get; set; }
        public virtual DbSet<ReturProduct> ReturProducts { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<TransactionDetail> TransactionDetails { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "en_US.UTF-8");

            modelBuilder.Entity<Blob>(entity =>
            {
                entity.ToTable("blob");

                entity.Property(e => e.BlobId)
                    .ValueGeneratedNever()
                    .HasColumnName("blob_id");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasColumnName("file_name");

                entity.Property(e => e.FilePath)
                    .IsRequired()
                    .HasColumnName("file_path");

                entity.Property(e => e.MimeType)
                    .IsRequired()
                    .HasColumnName("mime_type");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("category");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category_id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.CategoryName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("category_name");
            });

            modelBuilder.Entity<Delivery>(entity =>
            {
                entity.ToTable("delivery");

                entity.Property(e => e.DeliveryId)
                    .ValueGeneratedNever()
                    .HasColumnName("delivery_id");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .HasColumnName("address");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DeliveryStatusId).HasColumnName("delivery_status_id");

                entity.Property(e => e.TransactionId).HasColumnName("transaction_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.DeliveryStatus)
                    .WithMany(p => p.Deliveries)
                    .HasForeignKey(d => d.DeliveryStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("d__delivery_status_fk");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.Deliveries)
                    .HasForeignKey(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("d__transaction_fk");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Deliveries)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("d__user_fk");
            });

            modelBuilder.Entity<DeliveryStatus>(entity =>
            {
                entity.ToTable("delivery_status");

                entity.Property(e => e.DeliveryStatusId)
                    .HasColumnName("delivery_status_id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.DeliveryStatusName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("delivery_status_name");
            });

            modelBuilder.Entity<PaymentHistory>(entity =>
            {
                entity.ToTable("payment_history");

                entity.Property(e => e.PaymentHistoryId)
                    .ValueGeneratedNever()
                    .HasColumnName("payment_history_id");

                entity.Property(e => e.Payment).HasColumnName("payment");

                entity.Property(e => e.PaymentDate)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("payment_date")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.TransactionId).HasColumnName("transaction_id");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.PaymentHistories)
                    .HasForeignKey(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ph__transaction");
            });

            modelBuilder.Entity<PaymentType>(entity =>
            {
                entity.ToTable("payment_type");

                entity.Property(e => e.PaymentTypeId)
                    .HasColumnName("payment_type_id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.PaymentTypeName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("payment_type_name");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("product");

                entity.Property(e => e.ProductId)
                    .ValueGeneratedNever()
                    .HasColumnName("product_id");

                entity.Property(e => e.BarcodeId)
                    .HasMaxLength(10)
                    .HasColumnName("barcode_id");

                entity.Property(e => e.BasePrice).HasColumnName("base_price");

                entity.Property(e => e.BlobId).HasColumnName("blob_id");

                entity.Property(e => e.CategoryId).HasColumnName("category_id");

                entity.Property(e => e.ImageName)
                    .HasColumnType("character varying")
                    .HasColumnName("image_name");

                entity.Property(e => e.MinimumStockLimit)
                    .HasColumnName("minimum_stock_limit")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("product_name");

                entity.Property(e => e.Stock).HasColumnName("stock");

                entity.Property(e => e.SupplierId).HasColumnName("supplier_id");

                entity.Property(e => e.UnitId).HasColumnName("unit_id");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("updated_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(d => d.Blob)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.BlobId)
                    .HasConstraintName("p_blob_fk");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("p__category_fk");

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.SupplierId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("p__supplier_fk");

                entity.HasOne(d => d.Unit)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.UnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("p__unit_fk");
            });

            modelBuilder.Entity<PurchaseOrder>(entity =>
            {
                entity.ToTable("purchase_order");

                entity.Property(e => e.PurchaseOrderId)
                    .ValueGeneratedNever()
                    .HasColumnName("purchase_order_id");

                entity.Property(e => e.ApprovedAt)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("approved_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note)
                    .HasMaxLength(255)
                    .HasColumnName("note");

                entity.Property(e => e.PurchaseOrderStatusId).HasColumnName("purchase_order_status_id");

                entity.Property(e => e.SubmitBy)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("submit_by");

                entity.Property(e => e.SupplierId).HasColumnName("supplier_id");

                entity.Property(e => e.TotalPrice).HasColumnName("total_price");

                entity.HasOne(d => d.PurchaseOrderStatus)
                    .WithMany(p => p.PurchaseOrders)
                    .HasForeignKey(d => d.PurchaseOrderStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("po__purchase_order_status_fk");

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.PurchaseOrders)
                    .HasForeignKey(d => d.SupplierId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("po__supplier_fk");
            });

            modelBuilder.Entity<PurchaseOrderDetail>(entity =>
            {
                entity.ToTable("purchase_order_detail");

                entity.Property(e => e.PurchaseOrderDetailId)
                    .ValueGeneratedNever()
                    .HasColumnName("purchase_order_detail_id");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.Property(e => e.PurchaseOrderId).HasColumnName("purchase_order_id");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.ReturQuantity).HasColumnName("retur_quantity");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.PurchaseOrderDetails)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("pod__product_fk");

                entity.HasOne(d => d.PurchaseOrder)
                    .WithMany(p => p.PurchaseOrderDetails)
                    .HasForeignKey(d => d.PurchaseOrderId)
                    .HasConstraintName("pod_purchase_order_fk");
            });

            modelBuilder.Entity<PurchaseOrderStatus>(entity =>
            {
                entity.ToTable("purchase_order_status");

                entity.Property(e => e.PurchaseOrderStatusId)
                    .HasColumnName("purchase_order_status_id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.PurchaseOrderStatusName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("purchase_order_status_name");
            });

            modelBuilder.Entity<ReturProduct>(entity =>
            {
                entity.ToTable("retur_product");

                entity.Property(e => e.ReturProductId)
                    .ValueGeneratedNever()
                    .HasColumnName("retur_product_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DeliveryId).HasColumnName("delivery_id");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.TransactionId).HasColumnName("transaction_id");

                entity.HasOne(d => d.Delivery)
                    .WithMany(p => p.ReturProducts)
                    .HasForeignKey(d => d.DeliveryId)
                    .HasConstraintName("rp__delivery_fk");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ReturProducts)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("rp__product_fk");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.ReturProducts)
                    .HasForeignKey(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("rp__transaction_fk");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");

                entity.Property(e => e.RoleId)
                    .HasColumnName("role_id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("role_name");
            });

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.ToTable("supplier");

                entity.Property(e => e.SupplierId)
                    .ValueGeneratedNever()
                    .HasColumnName("supplier_id");

                entity.Property(e => e.SupplierEmail)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("supplier_email");

                entity.Property(e => e.SupplierName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("supplier_name");

                entity.Property(e => e.SupplierPhone)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("supplier_phone");
            });

            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.ToTable("transaction");

                entity.Property(e => e.TransactionId)
                    .ValueGeneratedNever()
                    .HasColumnName("transaction_id");

                entity.Property(e => e.BuyerName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("buyer_name");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DeadlineDate)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("deadline_date")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Interest).HasColumnName("interest");

                entity.Property(e => e.IsPaid).HasColumnName("is_paid");

                entity.Property(e => e.PaymentTypeId).HasColumnName("payment_type_id");

                entity.Property(e => e.TotalPrice).HasColumnName("total_price");

                entity.HasOne(d => d.PaymentType)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.PaymentTypeId)
                    .HasConstraintName("tr__payment_type_fk");
            });

            modelBuilder.Entity<TransactionDetail>(entity =>
            {
                entity.ToTable("transaction_detail");

                entity.Property(e => e.TransactionDetailId)
                    .ValueGeneratedNever()
                    .HasColumnName("transaction_detail_id");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.TransactionId).HasColumnName("transaction_id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TransactionDetails)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("td__product_fk");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.TransactionDetails)
                    .HasForeignKey(d => d.TransactionId)
                    .HasConstraintName("td__transaction_fk");
            });

            modelBuilder.Entity<Unit>(entity =>
            {
                entity.ToTable("unit");

                entity.Property(e => e.UnitId)
                    .HasColumnName("unit_id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.CategoryId).HasColumnName("category_id");

                entity.Property(e => e.UnitName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("unit_name");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Units)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("un__category_fk");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.UserId)
                    .ValueGeneratedNever()
                    .HasColumnName("user_id");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("email");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("name");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("password");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("username");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("u__role_fk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
