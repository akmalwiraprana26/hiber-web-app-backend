﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TokoBangunanHiber.Entities.Enums
{
    public enum DeliveryStatusEnum
    {
        [Display(Name = "Memuat Barang")]
        MemuatBarang = 1,

        [Display(Name = "Menuju Customer")]
        MenujuCustomer,

        [Display(Name = "Barang Sampai")]
        BarangSampai
    }
}
